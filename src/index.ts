const result = document.getElementById("result") as HTMLInputElement;
const zero = document.getElementById("zero") as HTMLButtonElement;
const un = document.getElementById("un") as HTMLButtonElement;
const deux = document.getElementById("deux") as HTMLButtonElement;
const trois = document.getElementById("trois") as HTMLButtonElement;
const quattre = document.getElementById("quattre") as HTMLButtonElement;
const cinq = document.getElementById("cinq") as HTMLButtonElement;
const six = document.getElementById("six") as HTMLButtonElement;
const sept = document.getElementById("sept") as HTMLButtonElement;
const huit = document.getElementById("huit") as HTMLButtonElement;
const neuf = document.getElementById("neuf") as HTMLButtonElement;
const multiplication = document.getElementById("multiplication") as HTMLButtonElement;
const division = document.getElementById("division") as HTMLButtonElement;
const addition = document.getElementById("addition") as HTMLButtonElement;
const sustraction = document.getElementById("sustraction") as HTMLButtonElement;
const equal = document.getElementById("equals") as HTMLButtonElement;
const supprimer = document.getElementById("supprimer") as HTMLButtonElement;
const affichage = document.getElementById("affichage") as HTMLLabelElement;
const operation=document.getElementById("operation") as HTMLLabelElement;
let firstNumber = 0;
let operator = "";

function appendNumber(number: string) {
    result.value += number; // fonction qui laisse les nombres affichés sans écraser le précedent
    }
function setOperator(newOperator: string) {
    firstNumber = parseFloat(result.value);
    operator = newOperator;
    result.value = "";
}

function clearResult() {
    result.value = "";
}
function calculate() {
    const secondNumber = parseFloat(result.value);
    let resultValue = 0;

    if (operator === "+") {
        resultValue = firstNumber + secondNumber;
    }
    else if (operator === "-") {
        resultValue = firstNumber - secondNumber;
    }
    else if (operator === "/") {
        resultValue = firstNumber / secondNumber;
    }
    else if (operator === "*") {
        resultValue = firstNumber * secondNumber;
    }
   //

    zero.addEventListener("click", () => {
        affichage.textContent += "0";
    });
    console.log(zero);

    un.addEventListener("click", () => {
        affichage.textContent += "1"
    })

    deux.addEventListener("click", () => {
        affichage.textContent += "2"
    })

    trois.addEventListener("click", () => {
        affichage.textContent += "3"
    })

    quattre.addEventListener("click", () => {
        affichage.textContent += "4"
    })
    cinq.addEventListener("click", () => {
        affichage.textContent += "5"
    })

    six.addEventListener("click", () => {
        affichage.textContent += "6"
    })
    sept.addEventListener("click", () => {
        affichage.textContent += "7"
    })
    huit.addEventListener("click", () => {
        affichage.textContent += "8"
    })

    neuf.addEventListener("click", () => {
        affichage.textContent += "neuf"
    })

    addition.addEventListener("click", () => {
        setOperator("+");
    });

    sustraction.addEventListener("click", () => {
        setOperator("-");
    });

    multiplication.addEventListener("click", () => {
        setOperator("*");
    });

    division.addEventListener("click", () => {
        setOperator("/");
    });

    equal.addEventListener("click", () => {
        calculate();
    });

    supprimer.addEventListener("click", () => {
        clearResult();
        affichage.textContent = "";
    })
}
