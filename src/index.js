var result = document.getElementById("result");
var zero = document.getElementById("zero");
var un = document.getElementById("un");
var deux = document.getElementById("deux");
var trois = document.getElementById("trois");
var quattre = document.getElementById("quattre");
var cinq = document.getElementById("cinq");
var six = document.getElementById("six");
var sept = document.getElementById("sept");
var huit = document.getElementById("huit");
var neuf = document.getElementById("neuf");
var multiplication = document.getElementById("multiplication");
var division = document.getElementById("division");
var addition = document.getElementById("addition");
var sustraction = document.getElementById("sustraction");
var equal = document.getElementById("equals");
var supprimer = document.getElementById("supprimer");
var affichage = document.getElementById("affichage");
var firstNumber = 0;
var operator = "";
function appendNumber(number) {
    result.value += number; // fonction qui laisse les nombres affichés sans écraser le précedent
}
function setOperator(newOperator) {
    firstNumber = parseFloat(result.value);
    operator = newOperator;
    result.value = "";
}
function clearResult() {
    result.value = "";
}
function calculate() {
    var secondNumber = parseFloat(result.value);
    var resultValue = 0;
    if (operator === "+") {
        resultValue = firstNumber + secondNumber;
    }
    else if (operator === "-") {
        resultValue = firstNumber - secondNumber;
    }
    else if (operator === "/") {
        resultValue = firstNumber / secondNumber;
    }
    else if (operator === "*") {
        resultValue = firstNumber * secondNumber;
    }
    //
    zero.addEventListener("click", function () {
        affichage.textContent += "0";
    });
    un.addEventListener("click", function () {
        affichage.textContent += "1";
    });
    deux.addEventListener("click", function () {
        affichage.textContent += "2";
    });
    trois.addEventListener("click", function () {
        affichage.textContent += "3";
    });
    quattre.addEventListener("click", function () {
        affichage.textContent += "4";
    });
    cinq.addEventListener("click", function () {
        affichage.textContent += "5";
    });
    six.addEventListener("click", function () {
        affichage.textContent += "6";
    });
    sept.addEventListener("click", function () {
        affichage.textContent += "7";
    });
    huit.addEventListener("click", function () {
        affichage.textContent += "8";
    });
    neuf.addEventListener("click", function () {
        affichage.textContent += "neuf";
    });
    addition.addEventListener("click", function () {
        setOperator("+");
    });
    sustraction.addEventListener("click", function () {
        setOperator("-");
    });
    multiplication.addEventListener("click", function () {
        setOperator("*");
    });
    division.addEventListener("click", function () {
        setOperator("/");
    });
    equal.addEventListener("click", function () {
        calculate();
    });
    supprimer.addEventListener("click", function () {
        clearResult();
        affichage.textContent = "";
    });
}
//# sourceMappingURL=index.js.map