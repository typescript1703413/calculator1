var result = document.getElementById("result");
var btn0 = document.getElementById("btn0");
var btn1 = document.getElementById("btn1");
var btn2 = document.getElementById("btn2");
var btn3 = document.getElementById("btn3");
var btn4 = document.getElementById("btn4");
var btn5 = document.getElementById("btn5");
var btn6 = document.getElementById("btn6");
var btn7 = document.getElementById("btn7");
var btn8 = document.getElementById("btn8");
var btn9 = document.getElementById("btn9");
var btnAdd = document.getElementById("btnAdd");
var btnSubtract = document.getElementById("btnSubtract");
var btnMultiply = document.getElementById("btnMultiply");
var btnDivide = document.getElementById("btnDivide");
var btnClear = document.getElementById("btnClear");
var btnCalculate = document.getElementById("btnCalculate");
var firstNumber = 0;
var operator = "";
function appendNumber(number) {
    result.value += number; // fonction qui laisse les nombres affichés sans écraser le précedent
}
function setOperator(newOperator) {
    firstNumber = parseFloat(result.value);
    operator = newOperator;
    result.value = "";
}
function clearResult() {
    result.value = "";
}
function calculate() {
    var secondNumber = parseFloat(result.value);
    var resultValue = 0;
    if (operator === "+") {
        resultValue = firstNumber + secondNumber;
    }
    else if (operator === "-") {
        resultValue = firstNumber - secondNumber;
    }
    else if (operator === "/") {
        resultValue = firstNumber / secondNumber;
    }
    else if (operator === "*") {
        resultValue = firstNumber * secondNumber;
    }
    zero.addEventListener("click", () => {
        affichage.textContent += "0";
    });

    un.addEventListener("click", () => {
        affichage.textContent += "1"
    })

    deux.addEventListener("click", () => {
        affichage.textContent += "2"
    })

    trois.addEventListener("click", () => {
        affichage.textContent += "3"
    })

    quattre.addEventListener("click", () => {
        affichage.textContent += "4"
    })
    cinq.addEventListener("click", () => {
        affichage.textContent += "5"
    })

    six.addEventListener("click", () => {
        affichage.textContent += "6"
    })
    sept.addEventListener("click", () => {
        affichage.textContent += "7"
    })
    huit.addEventListener("click", () => {
        affichage.textContent += "8"
    })

    neuf.addEventListener("click", () => {
        affichage.textContent += "neuf"
    })

    addition.addEventListener("click", () => {
        setOperator("+");
    });

    sustraction.addEventListener("click", () => {
        setOperator("-");
    });

    multiplication.addEventListener("click", () => {
        setOperator("*");
    });

    division.addEventListener("click", () => {
        setOperator("/");
    });

    equal.addEventListener("click", () => {
        calculate();
    });

    supprimer.addEventListener("click", () => {
        clearResult();
        affichage.textContent = "";
    })
}
